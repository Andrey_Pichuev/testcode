﻿using Naxeex.Injectro.Core;

namespace Naxeex.Top.Global.CraftSystem
{
    public interface ICraftManager : IBean
    {
        void CraftCurrentItem();
        void UpdateCategories();
    }
}
