﻿using System.Collections.Generic;
using System.Text;
using Naxeex.Injectro.Core;
using Naxeex.Top.Common.Database;
using Naxeex.Top.Common.Item;
using Naxeex.Top.Common.Items;
using Naxeex.Top.Global.CraftSystem.UI;
using Naxeex.Top.Global.Localization;
using Naxeex.Top.Global.RewardUIGlobal;
using Naxeex.Top.Global.Sound;
using Naxeex.Top.Global.Storage;
using Naxeex.Top.Mission.RewardUI;
using UnityEngine;

namespace Naxeex.Top.Global.CraftSystem
{
    [Singleton]
    public class CraftManager : MonoBehaviour, ICraftManager ,IPostResolved
    {
        private const string RecipeTextKey = "RECIPE";
        private const string ColonAndSeparator = ": ";
        private const string Separator = " ";
        private const string CountTextKey = "COUNT";
        private const string CantCraftedTextKey = "CantCraft";
        private const string AlreadyCraftedTextKey = "AlreadyCrafted";

        private readonly StringBuilder builder = new StringBuilder();

        [Inject] private ICraftView craftView;
        [Inject] private IDatabaseManager databaseManager;
        [Inject] private IMenuRewardView rewardView;
        [Inject] private IStorageManager storageManager;
        [Inject] private ILocalizationManager localizationManager;
        [Inject] private ISoundManager soundManager;

        [SelectiveString("SoundTypes"), SerializeField]
        private string craftSoundId;
        
        private List<CraftResourceInfo> craftResourceInfos;
        private List<CraftRecipeInfo> craftRecipeInfos;

        private string currentRecipe;
        private bool hasCraftButton = true;

        public void PostResolved()
        {
            craftResourceInfos = databaseManager.GetAllInfoByType<CraftResourceInfo>();
            craftRecipeInfos = databaseManager.GetAllInfoByType<CraftRecipeInfo>();
        }

        public void CraftCurrentItem()
        {
            if (!CanCraft(currentRecipe))
            {
                return;
            }

            List<RewardItem> rewardItems = new List<RewardItem>();
            CraftRecipeInfo recipe = GetRecipeInfoByName(currentRecipe);
            
            GetItemDetails(recipe, out var itemName, out var itemTitle, out _, out var itemSprite);
            
            DepriveResource(recipe.GetName());

            RewardItem rewardItem = new RewardItem()
            {
                Amount = recipe.GetResultItemCount(),
                Icon = itemSprite,
                ItemName = itemName,
                LocalizedTitle = itemTitle,
                IsPlural = false,
            };

            StorageManager.Reward reward = new StorageManager.Reward()
            {
                Count = recipe.GetResultItemCount(),
                RewardName = itemName,
            };

            soundManager.Play(craftSoundId);
            storageManager.GiveReward(reward);
            rewardItems.Add(rewardItem);
            rewardView.Open(rewardItems);
            rewardItems.Clear();
            UpdateParameters(currentRecipe);
        }

        public void UpdateCategories()
        {
            for (int i = 0; i < craftRecipeInfos.Count; i++)
            {
                GetItemDetails(craftRecipeInfos[i], out _, out var itemTitle, out _, out var itemSprite);

                craftView.CreateRecipeButtonViews(craftRecipeInfos[i].GetName(),
                    itemTitle, 
                    itemSprite,
                    UpdateParameters);
            }
            
            UpdateParameters(craftRecipeInfos[0].GetName());
        }
        
        private void UpdateParameters(string recipeName)
        {
            CraftRecipeInfo recipeInfo = GetRecipeInfoByName(recipeName);

            if (recipeInfo == null)
            {
                return;
            }
            
            GetItemDetails(recipeInfo, out _, out var itemTitle, out var itemDescription, out var itemSprite);

            craftView.UpdateScroll(recipeName);
            currentRecipe = recipeName;

            builder.Clear();
            builder.Append(itemTitle).Append(Separator)
                .Append(localizationManager.Translate(RecipeTextKey));
                
            craftView.SetRecipeTitleAndDescription(builder.ToString(), itemDescription);
            
            craftView.ClearData();

            bool alreadyCrafted = false;
            bool canCraft = CanCraft(recipeName);
            if (!hasCraftButton)
            {
                hasCraftButton = true;
                craftView.SetCraftButtonActive(true);
            }
            
            craftView.SetCraftButtonState(canCraft);

            if (canCraft)
            {
                builder.Clear();
                builder.Append(localizationManager.Translate(CountTextKey)).Append(ColonAndSeparator).Append(recipeInfo.GetResultItemCount());
                craftView.SetCountResultItemText(builder.ToString());
            }
            else
            {
                if (recipeInfo.CraftOneTime && storageManager.IsItemBought(recipeInfo.GetCraftResultItemName()))
                {
                    alreadyCrafted = true;
                    hasCraftButton = false;
                    craftView.SetCraftButtonActive(false);
                    craftView.SetCountResultItemText(localizationManager.Translate(AlreadyCraftedTextKey));
                }
                else
                {
                    craftView.SetCountResultItemText(localizationManager.Translate(CantCraftedTextKey));
                }
            }
            
            craftView.SetCurrentItemSprite(itemSprite);

            for (int i = 0; i < craftResourceInfos.Count; i++)
            {
                string resourceId = craftResourceInfos[i].GetName();
                craftView.CreateResourceView(
                    localizationManager.Translate(resourceId), 
                    storageManager.GetItemCount(resourceId),
                    GetResourceByName(resourceId).GetIcon());
            }
            
            if (alreadyCrafted)
            {
                return;
            }
            
            CraftRecipeInfo.RecipeResource[] recipeResources = recipeInfo.GetRecipeResources();
            for (int i = 0; i < recipeResources.Length; i++)
            {
                string resourceId = recipeResources[i].ResourceId;
                craftView.CreateRequirementView(
                    localizationManager.Translate(resourceId),
                    GetResourceByName(resourceId).GetIcon(),
                    storageManager.GetItemCount(resourceId), 
                    recipeResources[i].Count);
            }
        }
        
        private ShopItemInfo GetCraftItemInfo(string itemName)
        {
            return databaseManager.GetInfo<ShopItemInfo>(itemName);
        }

        private CurrencyInfo GetCurrencyInfo(string currencyName)
        {
            return databaseManager.GetInfo<CurrencyInfo>(currencyName);
        }

        private void GetItemDetails(CraftRecipeInfo craftRecipeInfo, out string itemName, out string itemTitle, out string itemDescription, out Sprite itemIcon)
        {
            string craftResultItemId = craftRecipeInfo.GetCraftResultItemName();

            if (craftRecipeInfo.IsCurrency)
            {
                CurrencyInfo currencyInfo = GetCurrencyInfo(craftResultItemId);
                itemName = currencyInfo.GetName();
                itemTitle = localizationManager.Translate(currencyInfo.TitleKey);
                itemDescription = localizationManager.Translate(currencyInfo.DescriptionKey);
                itemIcon = currencyInfo.Icon;
            } else
            {
                ShopItemInfo shopItemInfo = GetCraftItemInfo(craftResultItemId);
                itemName = shopItemInfo.GetName();
                itemTitle = localizationManager.Translate(shopItemInfo.TitleKey);
                itemDescription = localizationManager.Translate(shopItemInfo.DescriptionKey);
                itemIcon = shopItemInfo.Icon;
            }
        }

        private CraftRecipeInfo GetRecipeInfoByName(string name)
        {
            for (int i = 0; i < craftRecipeInfos.Count; i++)
            {
                if (craftRecipeInfos[i].GetName() == name)
                {
                    return craftRecipeInfos[i];
                }
            }
            
            return null;
        }

        private CraftResourceInfo GetResourceByName(string name)
        {
            for (int i = 0; i < craftResourceInfos.Count; i++)
            {
                if (craftResourceInfos[i].GetName() == name)
                {
                    return craftResourceInfos[i];
                }
            }
            
            return null;
        }

        private bool CanCraft(string recipeName)
        {
            CraftRecipeInfo recipe = GetRecipeInfoByName(recipeName);
            string resultCraftId = recipe.GetCraftResultItemName();

            if (recipe.CraftOneTime && storageManager.IsItemBought(resultCraftId))
            {
                return false;
            }

            CraftRecipeInfo.RecipeResource[] resources = recipe.GetRecipeResources();
            for (int i = 0; i < resources.Length;  i++)
            {
                int count = storageManager.GetItemCount(resources[i].ResourceId);
                if (count < resources[i].Count)
                {
                    return false;
                }
            }

            return true;
        }

        private void DepriveResource(string recipeId)
        {
            CraftRecipeInfo recipe = GetRecipeInfoByName(recipeId);
            CraftRecipeInfo.RecipeResource[] resources = recipe.GetRecipeResources();
            for (int i = 0; i < resources.Length;  i++)
            {
                int count = storageManager.GetItemCount(resources[i].ResourceId);
                storageManager.SetItemCount(resources[i].ResourceId, count - resources[i].Count);
            }
        }
    }
}
