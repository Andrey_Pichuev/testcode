﻿using System;
using Naxeex.Top.Common.Database;
using UnityEngine;

namespace Naxeex.Top.Global.CraftSystem
{
    [Serializable]
    [CreateAssetMenu(fileName = "CraftRecipe", menuName = "Configs/Craft/CraftRecipeInfo", order = 0)]
    public class CraftRecipeInfo : ScriptableObject, IInfo
    {
        [SelectiveString("CraftRecipes"), SerializeField]
        private string craftRecipeName;

        [SelectiveString("Weapon", "Item", "Ammo", "Buff", "Currency"), SerializeField]
        private string craftResultInfoName;

        [SerializeField] private int resultItemCount = 1;

        [SerializeField] private RecipeResource[] recipeResource;

        [SerializeField] private bool craftOneTime;
        [SerializeField] private bool isCurrency;

        public bool CraftOneTime => craftOneTime;

        public bool IsCurrency => isCurrency;

        public string GetName()
        {
            return craftRecipeName;
        }

        public string GetCraftResultItemName()
        {
            return craftResultInfoName;
        }

        public RecipeResource[] GetRecipeResources()
        {
            return recipeResource;
        }

        public int GetResultItemCount()
        {
            return resultItemCount;
        }
        
        [Serializable]
        public struct RecipeResource
        {
            [SelectiveString("CraftResources")] public string ResourceId;
            [Range(1, 5000)] public int Count;
        }
    }
}
